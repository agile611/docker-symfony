# Docker Symfony (PHP7-FPM - NGINX - MySQL - ELK)

[![Build Status](https://travis-ci.org/maxpou/docker-symfony.svg?branch=master)](https://travis-ci.org/maxpou/docker-symfony)

![](doc/schema.png)

Docker-symfony gives you everything you need for developing Symfony application. This complete stack run with docker and [docker-compose (1.7 or higher)](https://docs.docker.com/compose/).

I also added the following [php-cli](https://hub.docker.com/r/seblegall/php-cli-docker)

## Usage locally

Just run `docker-compose -f docker-compose-v3.yml up -d`, then:

* Symfony app: visit [symfony.dev](http://symfony.dev)  
* Symfony dev mode: visit [symfony.dev/app_dev.php](http://symfony.dev/app_dev.php)  
* Logs (Kibana): [symfony.dev:81](http://symfony.dev:81)
* Logs (files location): logs/nginx and logs/symfony


## Deploy Stack

You have to create a Swarm, I tested using 1 manager and 2 workers.

On node 1, run `docker swarm init`

If you want you can select if a joining node is a worker or a manager:

* For a worker node, you have to run `docker swarm join-token worker`
* For a manager node, `docker swarm join-token manager`

The line for a joining node is as follows:

* On node 2, run `docker swarm join --token SWMTKN-1-0wf5snnb2ejt0zkpmcrfilj2lnwisknj41qt4nv0mqkedmo1hs-4fi5b2yol74uc45tk6ikrmw58 192.168.65.2:2377`
* On node 3, run `docker swarm join --token SWMTKN-1-0wf5snnb2ejt0zkpmcrfilj2lnwisknj41qt4nv0mqkedmo1hs-4fi5b2yol74uc45tk6ikrmw58 192.168.65.2:2377` 

Running the stack when you have the swarm:

* Just run `docker stack deploy -c docker-stack.yml nginx-php` to start the stack
* Just run `docker stack rm nginx-php` to stop the stack

On a node, you will see the following services running:

```bash
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS               NAMES
54f1c8e74ba6        local_php:latest      "docker-php-entryp..."   11 seconds ago      Up 10 seconds       9000/tcp            nginx-php_php.1.2ogq0sh6c4q30dzsnjg91xzyn
e13ff101e86a        local_nginx:latest    "nginx"                  12 seconds ago      Up 7 seconds        80/tcp, 443/tcp     nginx-php_nginx.3.8otjd92katpf32fezhu9as9qy
1ffd0df56fb9        redis:3.2.10-alpine   "docker-entrypoint..."   14 seconds ago      Up 13 seconds       6379/tcp            nginx-php_redis.3.909eo2q0ljjszupw4byk40r78
```

On the swarm, you will see the following services running:
```bash
$ docker service ls
  ID                  NAME                MODE                REPLICAS            IMAGE                 PORTS
  v554qwo8jpz1        nginx-php_nginx     replicated          3/3                 local_nginx:latest    *:80->80/tcp
  ymuqfef165m9        nginx-php_redis     replicated          3/3                 redis:3.2.10-alpine   
  zb9wdwg06kup        nginx-php_php       replicated          3/3                 local_php:latest   
```

## Customize

If you want to add optionnals containers like Redis, PHPMyAdmin... take a look on [doc/custom.md](doc/custom.md).

## How it works?

Have a look at the `docker-compose.yml` file, here are the `docker-compose` built images:

* `db`: This is the MySQL database container,
* `php`: This is the PHP-FPM container in which the application volume is mounted,
* `nginx`: This is the Nginx webserver container in which application volume is mounted too,
* `elk`: This is a ELK stack container which uses Logstash to collect logs, send them into Elasticsearch and visualize them with Kibana.

This results in the following running containers:

```bash
$ docker-compose ps
           Name                          Command               State              Ports            
--------------------------------------------------------------------------------------------------
dockersymfony_db_1            /entrypoint.sh mysqld            Up      0.0.0.0:3306->3306/tcp      
dockersymfony_elk_1           /usr/bin/supervisord -n -c ...   Up      0.0.0.0:81->80/tcp          
dockersymfony_nginx_1         nginx                            Up      443/tcp, 0.0.0.0:80->80/tcp
dockersymfony_php_1           php-fpm                          Up      0.0.0.0:9000->9000/tcp      
```

## Useful commands

```bash
# bash commands
$ docker-compose exec php bash

# Composer (e.g. composer update)
$ docker-compose exec php composer update

# SF commands (Tips: there is an alias inside php container)
$ docker-compose exec php php /var/www/symfony/app/console cache:clear # Symfony2
$ docker-compose exec php php /var/www/symfony/bin/console cache:clear # Symfony3
# Same command by using alias
$ docker-compose exec php bash
$ sf cache:clear

# Retrieve an IP Address (here for the nginx container)
$ docker inspect --format '{{ .NetworkSettings.Networks.dockersymfony_default.IPAddress }}' $(docker ps -f name=nginx -q)
$ docker inspect $(docker ps -f name=nginx -q) | grep IPAddress

# MySQL commands
$ docker-compose exec db mysql -uroot -p"root"

# F***ing cache/logs folder
$ sudo chmod -R 777 app/cache app/logs # Symfony2
$ sudo chmod -R 777 var/cache var/logs var/sessions # Symfony3

# Check CPU consumption
$ docker stats $(docker inspect -f "{{ .Name }}" $(docker ps -q))

# Delete all containers
$ docker rm $(docker ps -aq)

# Delete all images
$ docker rmi $(docker images -q)
```

## FAQ

* Got this error: `ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?
If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.` ?  
Run `docker-compose up -d` instead.

* Permission problem? See [this doc (Setting up Permission)](http://symfony.com/doc/current/book/installation.html#checking-symfony-application-configuration-and-setup)

* How to config Xdebug?
Xdebug is configured out of the box!
Just config your IDE to connect port  `9001` and id key `PHPSTORM`

## Contributing

First of all, **thank you** for contributing ♥  
If you find any typo/misconfiguration/... please send me a PR or open an issue. You can also ping me on [twitter](https://twitter.com/_maxpou).  
Also, while creating your Pull Request on GitHub, please write a description which gives the context and/or explains why you are creating it.
